package com.tmdt.thuephongtro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThuephongtroApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThuephongtroApplication.class, args);
    }

}
